import requests
import json
import streamlit as st

url = 'http://172.26.0.19:8000/api/predict/' # 'http://0.0.0.0:7010/api/predict/'
headers = {
    'accept': 'application/json',
    'Content-Type': 'application/json'
}

st.session_state["txt_to_pred_input"] = st.text_input('Текст для анализа', key='txt_for_pred')


def get_predict(txt_for_pred):
    print(txt_for_pred)
    payload = {
        'text': txt_for_pred  # 'Ура я сделал это задание, которое долго не получалось'
    }
    res = requests.post(url, data=json.dumps(payload), headers=headers)     # ,json=payload
    response_json = json.loads(res.text)    # res.json()
    st.write(f"Эмоция высказывания: \n{response_json[0]['label']}  \nс вероятностью: \n{response_json[0]['score']}")  # result.get()


st.button("Получить класс эмоции", on_click=get_predict, args=(st.session_state["txt_to_pred_input"],))