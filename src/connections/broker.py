from celery import Celery

# import os

celery_app = Celery(
    # broker=f"amqp://guest:guest@{os.environ.get('host')}:5672",
    broker="amqp://guest:guest@172.26.0.17:5672",
    backend="redis://172.26.0.6:6379/0"  # 172.26.0.10/1
)