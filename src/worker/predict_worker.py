
from src.services.model import EmotionClassifier
from loguru import logger
from src.connections.broker import celery_app


@celery_app.task(name="predict")
def predict_emotion(text):
    """
    Предсказание эмоции по тексту.

    """
    try:
        result = EmotionClassifier.predict_emotion(text)
        return result
    except Exception as ex:
        logger.error([ex])
        return None
