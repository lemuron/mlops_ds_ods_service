FROM python:3.8-slim

WORKDIR /app

COPY pyproject.toml poetry.lock /app/

RUN pip install --no-cache-dir poetry=="1.5.0"

RUN poetry install --no-dev

RUN poetry add celery[redis]

RUN poetry add celery[rabbitmq]

COPY . /app

CMD ["poetry", "run", "celery", "-A", "src.worker.predict_worker", "worker", "-l", "INFO"]