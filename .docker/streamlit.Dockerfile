FROM python:3.10

WORKDIR /app

COPY . /app
COPY pyproject.toml poetry.lock /app/
ENV PATH=/opt/poetry/bin:$PATH

#ENV VIRTUAL_ENV=/app/.venv
#ENV PATH="$VIRTUAL_ENV/bin:$PATH"

RUN pip install --no-cache-dir poetry=="1.5.0"

RUN poetry config virtualenvs.in-project true

RUN poetry install --no-dev

EXPOSE 8551

ENTRYPOINT [".venv/bin/python", "-m", "streamlit", "run", "src/streamlit.py", "--server.port=8551", "--server.address=0.0.0.0", "--server.enableCORS","false"]